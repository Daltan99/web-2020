import { Box, CheckBox, FormField, Grommet, Text, TextInput } from "grommet";
import { Login } from './Login'
import { SendMailForm } from './SendMailForm'
import React, { useState } from "react";
import {
  Avatar,
  EditProfile,
  Favorite,
  Inbox,
  Sent,
  Spam,
  Trash,
} from "./Icons";

const Ads = () => (
  <Box
    margin={{
      left: "10px",
      right: "7px",
    }}
    style={{
      boxShadow:
        "0px 0.694444px 2.77778px rgba(0, 0, 0, 0.01), 0px 2.77778px 5.55556px rgba(0, 0, 0, 0.02), 0px 0.694444px 8.33333px rgba(0, 0, 0, 0.12)",
    }}
    round="3px"
    pad="12px"
    height="90px"
  >
    <Text size="12px">Купи кота</Text>
    <Box height="9px"></Box>
    <Text size="13px">)))</Text>
    <Box
      margin={{
        top: "auto",
      }}
    ></Box>
    <Text
      size="11px"
      color="#263238"
      style={{
        opacity: "0.5",
      }}
    >
      10 min
    </Text>
  </Box>
);

const Sidebar = () => {
  const [active, setActive] = useState("Входящие");

  return (
    <Box
      width="256px"
      height="100%"
      style={{
        boxShadow:
          "0px 1px 2px rgba(0, 0, 0, 0.16), 0px 2px 4px rgba(0, 0, 0, 0.12), 0px 1px 8px rgba(0, 0, 0, 0.1)",
        zIndex: 22,
        minWidth: "256px",
      }}
      round={{
        corner: "right",
        size: "8px",
      }}
    >
      <Box pad="32px 18px 22px 22px" direction="row">
        <Avatar />
        <Box
          margin={{
            right: "auto",
          }}
        ></Box>
        <EditProfile />
      </Box>
      {[
        {
          name: "Входящие",
          icon: <Inbox />,
        },
        {
          name: "Избранное",
          icon: <Favorite />,
        },

        {
          name: "Отправленные",
          icon: <Sent />,
        },

        {
          name: "Спам",
          icon: <Spam />,
        },

        {
          name: "Корзина",
          icon: <Trash />,
        },
      ].map(item => {
        return (
          <Box
            margin={{
              left: "10px",
              right: "6px",
              bottom: "8px",
            }}
            pad="8px"
            align="center"
            direction="row"
            round="4px"
            height="40px"
            onClick={() => setActive(item.name)}
            {...(item.name === active
              ? {
                background: "#E3F2FD",
              }
              : {})}
          >
            {item.icon}
            <Box width="32px"></Box>
            <Text
              size="14px"
              weight={500}
              {...(item.name === active
                ? {
                  color: "#2196F3",
                }
                : {})}
            >
              {item.name}
            </Text>
          </Box>
        );
      })}
      <Box height="27px"></Box>
      <Ads />
      <Box height="20px"></Box>
      <Ads />
    </Box>
  );
};

const LetterItem = ({
  sender,
  subject,
  createdAt,

  active,
}: {
  sender: string;
  subject: string;
  createdAt: string;

  active?: boolean;
}) => (
    <Box
      direction="row"
      height="40px"
      pad="0px 8px 0px 16px"
      align="center"
      style={{
        position: "relative",
      }}
      {...(active
        ? {
          background: "#E3F2FD",
        }
        : {})}
    >
      <CheckBox />

      <Box width="30px"></Box>
      <Text size="14px">{sender}</Text>
      <Box width="40px"></Box>
      <Text size="14px">{subject}</Text>
      <Box
        margin={{
          left: "auto",
        }}
      ></Box>
      <Text size="14px" color="#26323885">
        {createdAt}
      </Text>

      <Box
        height="2px"
        width="calc(100% - 16px)"
        pad="0px 8px"
        background="#263238"
        style={{
          opacity: "0.16",
          position: "absolute",
          bottom: 0,
          left: 8,
          // left: ,
          // right: 8,
        }}
      ></Box>
    </Box>
  );

const Main = () => (
  <Box width="100%">
    <FormField label="Поиск в почте">
      <TextInput placeholder="Поиск..." />
    </FormField>
    {Array.from({
      length: 5,
    }).map((item, i) => (
      <LetterItem
        sender="Firebase"
        subject="[Firebase] Client access to your Cloud Firestore database expiring in 3 day"
        active={i === 0}
        createdAt="12:11"
      />
    ))}
  </Box>
);

const MainPage = () => (
  <Box width="100%" height="100vh" direction="row">
    <Sidebar />
    <Main />
  </Box>
);

function App() {
  return (
    <Grommet>
      <MainPage />
      <Login />
      <SendMailForm />
    </Grommet>
  );
}

export default App;
