import { Box, Grid, Select, Text, TextArea, TextInput } from "grommet";
import { Close, Trash_in_mail } from "./Icons"
import React from "react";

const SendMailForm: React.FC = () => (
    <Box align="center" pad={{
        "bottom": "50px"
    }}>
        <Box direction="column" >
            <Box direction="row" background="dark-3" gap="large" pad="small">
                <Text>Новое сообщение</Text>
                <Box pad="xsmall" margin={{
                    left: "auto"
                }}>
                    <Close />
                </Box>
            </Box>

            <Box border={true}>
                <TextInput plain={true}
                    placeholder="Кому"
                />
                <Box border="horizontal">
                    <TextInput plain={true}
                        placeholder="Тема"
                    />
                </Box>
                <TextArea
                    placeholder="Сообщение"
                    style={{
                        width: "512px",
                        height: "256px"
                    }}
                    resize={false}
                    plain={true}>
                </TextArea>
                <Box direction="row" pad="small" margin={{ "top": "15px" }}>
                    <Select
                        options={['send']}
                    />
                    <Box pad="small" margin={{
                        left: "auto"
                    }}>
                        <Trash_in_mail />
                    </Box>
                </Box>
            </Box>

        </Box>
    </Box >
)

export { SendMailForm };