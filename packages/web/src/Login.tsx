import { Box, Button, CheckBox, Form, FormField, Text, TextInput } from "grommet";
import React from "react";

const Login: React.FC = () => (

    <Box align="center" basis="xxsmall"
        margin={{
            "vertical": "300px"
        }} >
        <Form >

            <FormField name="name" label="Email">
                <TextInput id="textinput-id" name="name" />
            </FormField>
            <FormField name="name" label="Password">
                <TextInput id="textinput-id1" name="name1" />
            </FormField>
            <Box direction="column"
                margin={{
                    "top": "15px"
                }}>

                <CheckBox label="Remember me" />
                <Button label="Sign in" margin={{
                    "top": "50px",
                    "bottom": "15px"
                }} />
            </Box>
            <Box direction="row" gap="large">
                <Text>Forgot password?</Text>
                <Text>Don`t have an account? Sign up</Text>
            </Box>
            <Box align="center" margin={{
                "top": "50px"
            }}>
                <Text>Copyright © </Text>
            </Box>
        </Form>
    </Box>
);

export { Login };